@echo off

set var_rep_dest=c:\winpe_x64
set var_rep_dest_win=%var_rep_dest%\winpe-build
set var_windows_aik_dir=c:\Program Files\Windows AIK
set var_emplacement_reseau_windows7x64=192.168.1.69
set var_repertoire_reseau_windows7x64=\\%var_emplacement_reseau_windows7x64%\tftp\images\windows\windows7prox64
set var_iso_image_name=windows7prox64.iso

echo|set /p="- Creation de l'archive                                               "
call copype amd64 %var_rep_dest_win% > nul
echo OK

echo|set /p="- Creation du fichier de boot                                         "
copy "%var_windows_aik_dir%\Tools\PETools\amd64\winpe.wim" %var_rep_dest_win%\ISO\Sources\Boot.wim > nul
echo OK

echo|set /p="- Creation du fichier d'image                                         "
copy "%var_windows_aik_dir%\Tools\amd64\Imagex.exe" %var_rep_dest_win%\ISO\ > nul
echo OK

echo|set /p="- Montage de l'image pour edition                                     "
dism /mount-wim /wimfile:%var_rep_dest_win%\ISO\Sources\Boot.wim /index:1 /mountdir:%var_rep_dest_win%\mount > nul
echo OK

echo|set /p="- Mise en francais                                                    "
dism /image:%var_rep_dest_win%\mount /Set-AllIntl:fr-FR > nul
echo OK

echo|set /p="- Creation du fichier d'initialisation windows                        "
set var_system_drive=SystemDrive
set var_winpeshl_path=%var_rep_dest_win%\mount\windows\system32\winpeshl.ini 

rem Remplissage du fichier
set var_fichier_init1=[LaunchApps]
set var_fichier_init2=wpeinit
set var_fichier_init3=%%%var_system_drive%%%\local\init.cmd
echo %var_fichier_init1% > %var_winpeshl_path%
echo %var_fichier_init2% >> %var_winpeshl_path%
echo %var_fichier_init3% >> %var_winpeshl_path%
echo OK

echo|set /p="- Creation du dossier de lancement windows                            "
set var_dossier_local=%var_rep_dest_win%\mount\local
set var_fichier_init=%var_dossier_local%\init.cmd
mkdir %var_dossier_local%
rem Remplissage du fichier
set var_fichier_launch1=@echo off
set var_fichier_launch2=echo Connexion au repertoire reseau...
set var_fichier_launch3=net use z: %var_repertoire_reseau_windows7x64%
set var_fichier_launch4=echo Lancement de l'installation...
set var_fichier_launch5=z:\setup.exe

echo %var_fichier_launch1% > %var_fichier_init%
echo %var_fichier_launch2% >> %var_fichier_init%
echo %var_fichier_launch3% >> %var_fichier_init%
echo %var_fichier_launch4% >> %var_fichier_init%
echo %var_fichier_launch5% >> %var_fichier_init%
echo OK

echo|set /p="- Suppression du fichier "bootfix.bin"                                "
del %var_rep_dest_win%\ISO\boot\bootfix.bin
echo OK

echo|set /p="- Validation des changements                                          "
dism /unmount-wim /mountdir:%var_rep_dest_win%\mount /commit > nul
echo OK

echo - Creation du fichier ISO
call oscdimg -n -b%var_rep_dest_win%\etfsboot.com %var_rep_dest_win%\ISO %var_rep_dest%\%var_iso_image_name% > nul
cd \ > null
rmdir /Q /S %var_rep_dest_win% > nul
cd %var_rep_dest% > nul
echo L'image "%var_iso_image_name%" a ete cree dans le repertoire "%var_rep_dest%"
pause